/*
 *	Create script Live Performance BotenVerhuur
 *	Simin Jacobs
 */
 
--Disable prompts when using &
SET DEFINE OFF

--Drop all tables and constraints
DROP TABLE Account 							CASCADE CONSTRAINTS;
DROP TABLE Artikel 							CASCADE CONSTRAINTS;
DROP TABLE Boot 							CASCADE CONSTRAINTS;
DROP TABLE BijkomendArtikel 				CASCADE CONSTRAINTS;
DROP TABLE Huurder 							CASCADE CONSTRAINTS;
DROP TABLE Huurcontract 					CASCADE CONSTRAINTS;
DROP TABLE Huurcontract_Boot 				CASCADE CONSTRAINTS;
DROP TABLE Huurcontract_BijkomendArtikel	CASCADE CONSTRAINTS;

--Drop all sequences
DROP SEQUENCE SEQ_Account;
DROP SEQUENCE SEQ_Artikel;
DROP SEQUENCE SEQ_Boot;
DROP SEQUENCE SEQ_BijkomendArtikel;
DROP SEQUENCE SEQ_Huurder;
DROP SEQUENCE SEQ_Huurcontract;

--Create tables
CREATE TABLE Account
(
	id					INTEGER PRIMARY KEY,
	naam				VARCHAR2(255) NOT NULL,
	wachtwoord			VARCHAR2(255) NOT NULL,
	rol					VARCHAR2(255) NOT NULL
);

CREATE TABLE Artikel
(
	id					INTEGER PRIMARY KEY,
	naam				VARCHAR2(255) NOT NULL,
	prijs				NUMBER(30,2) NOT NULL CHECK(prijs >= 0)
);

CREATE TABLE Boot
(
	id					INTEGER PRIMARY KEY,
	Artikel_id			INTEGER REFERENCES Artikel(id),
	type				VARCHAR(255) NOT NULL,
	tankInhoud			INTEGER NOT NULL
);

CREATE TABLE BijkomendArtikel
(
	id					INTEGER PRIMARY KEY,
	Artikel_id			INTEGER REFERENCES Artikel(id)
);

CREATE TABLE Huurder
(
	id 					INTEGER PRIMARY KEY,
	naam				VARCHAR2(255) NOT NULL,
	email 				VARCHAR2(255) NOT NULL
);

CREATE TABLE Huurcontract
(
	id					INTEGER PRIMARY KEY,
	Huurder_id			INTEGER REFERENCES Huurder(id),
	verhuurderNaam		VARCHAR2(255) NOT NULL,
	datumStart			DATE,
	datumEinde			DATE
);

CREATE TABLE Huurcontract_Artikel
(
	Huurcontract_id		INTEGER REFERENCES Huurcontract(id),
	Artikel_id			INTEGER REFERENCES Artikel(id),
	aantal				INTEGER NOT NULL,
	CONSTRAINT PK_Huurcontract_Artikel PRIMARY KEY(Huurcontract_id, Artikel_id)
);

CREATE TABLE Huurcontract_Boot
(
	Huurcontract_id		INTEGER REFERENCES Huurcontract(id),
	Boot_id				INTEGER REFERENCES Boot(id),
	aantal				INTEGER NOT NULL,
	CONSTRAINT PK_Huurcontract_Boot PRIMARY KEY(Huurcontract_id, Boot_id)
);

CREATE TABLE Huurcontract_BijkomendArtikel
(
	Huurcontract_id		INTEGER REFERENCES Huurcontract(id),
	BijkomendArtikel_id	INTEGER REFERENCES BijkomendArtikel(id),
	aantal				INTEGER NOT NULL,
	CONSTRAINT PK_Huurcontract_BijkomendArt PRIMARY KEY(Huurcontract_id, BijkomendArtikel_id)
);

-- Auto increment for Account --

CREATE SEQUENCE SEQ_Account
	START WITH 1
	INCREMENT BY 1
	nomaxvalue;
  
	create or replace trigger TRIGGER_Account
	before insert on Account
	for each row
	begin
	select SEQ_Account.nextval into :new.id from dual;
end;
/

-- Auto increment for Artikel --

CREATE SEQUENCE SEQ_Artikel
	START WITH 1
	INCREMENT BY 1
	nomaxvalue;
  
	create or replace trigger TRIGGER_Artikel
	before insert on Artikel
	for each row
	begin
	select SEQ_Artikel.nextval into :new.id from dual;
end;
/

-- Auto increment for Boot --

CREATE SEQUENCE SEQ_Boot
	START WITH 1
	INCREMENT BY 1
	nomaxvalue;
  
	create or replace trigger TRIGGER_Boot
	before insert on Boot
	for each row
	begin
	select SEQ_Boot.nextval into :new.id from dual;
end;
/

-- Auto increment for BijkomendArtikel --

CREATE SEQUENCE SEQ_BijkomendArtikel
	START WITH 1
	INCREMENT BY 1
	nomaxvalue;
  
	create or replace trigger TRIGGER_BijkomendArtikel
	before insert on BijkomendArtikel
	for each row
	begin
	select SEQ_BijkomendArtikel.nextval into :new.id from dual;
end;
/

-- Auto increment for Huurder --

CREATE SEQUENCE SEQ_Huurder
	START WITH 1
	INCREMENT BY 1
	nomaxvalue;
  
	create or replace trigger TRIGGER_Huurder
	before insert on Huurder
	for each row
	begin
	select SEQ_Huurder.nextval into :new.id from dual;
end;
/

-- Auto increment for Huurcontract --

CREATE SEQUENCE SEQ_Huurcontract
	START WITH 1
	INCREMENT BY 1
	nomaxvalue;
  
	create or replace trigger TRIGGER_Huurcontract
	before insert on Huurcontract
	for each row
	begin
	select SEQ_Huurcontract.nextval into :new.id from dual;
end;
/

-- Insert test Artikels --

INSERT INTO Artikel(id, naam, prijs)
VALUES(
	1,
	'Motorboot Kruiser', 
	15
);

INSERT INTO Boot(Artikel_id, type, tankInhoud)
VALUES(
	1, 
	'MotorbootKruiser', 
	30
);

INSERT INTO Artikel(id, naam, prijs)
VALUES(
	2,
	'Kano', 
	10
);

INSERT INTO Boot(Artikel_id, type, tankInhoud)
VALUES(
	2, 
	'Kano', 
	0
);

INSERT INTO Artikel(id, naam, prijs)
VALUES(
	3,
	'Zeilboot Valk', 
	10
);

INSERT INTO Boot(Artikel_id, type, tankInhoud)
VALUES(
	3, 
	'ZeilbootValk', 
	0
);

INSERT INTO Artikel(id, naam, prijs)
VALUES(
	4,
	'Zeilboot Laser', 
	10
);

INSERT INTO Boot(Artikel_id, type, tankInhoud)
VALUES(
	4, 
	'ZeilbootLaser', 
	0
);


-- Insert test Accounts --

INSERT INTO Account(naam, wachtwoord, rol)
VALUES(
	'simin', 
	'simin123',
	'Medewerker'
);

-- Insert test Huurders --

INSERT INTO Huurder(id, naam, email)
VALUES(
	1,
	'Jan', 
	'jan@gmail.com'
);

INSERT INTO Huurder(id, naam, email)
VALUES(
	2,
	'Kees', 
	'kees@gmail.com'
);

commit;