﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using LPSiminBotenVerhuur.Model;
using System.Collections.Generic;

namespace UnitTestProject
{
    [TestClass]
    public class UnitTest1
    {
        [TestMethod]
        public void TestAccount()
        {
            Account account = new Account("simin", "simin123", AccountRol.Medewerker);

            Artikel boot = new Boot(1, "boot", 20, BootType.Kano, 30);
            boot.ToString();

            Artikel bijkomendArt = new BijkomendArtikel(1, "vest", 5);

            ArtikelMetAantal bootMetAantal = new ArtikelMetAantal(boot, 5);
            ArtikelMetAantal bijArtMetAantal = new ArtikelMetAantal(bijkomendArt, 1);
            bootMetAantal.ToString();

            GevoelsTemperatuur gevoelsTemp = new GevoelsTemperatuur(DateTime.Today, 20);
            gevoelsTemp.ToString();

            List<ArtikelMetAantal> artikelenMetAantal = new List<ArtikelMetAantal>();
            artikelenMetAantal.Add(bootMetAantal);
            artikelenMetAantal.Add(bijArtMetAantal);

            Huurcontract huurContract = new Huurcontract("simin", new Huurder("henk", "henk@gmail.com"), artikelenMetAantal, DateTime.Now, DateTime.Now);
            huurContract.ExportToTxt();
            huurContract.ToString();
        }
    }
}
