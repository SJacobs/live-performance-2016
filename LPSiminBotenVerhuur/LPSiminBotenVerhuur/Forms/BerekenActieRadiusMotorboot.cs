﻿using LPSiminBotenVerhuur.Model;
using LPSiminBotenVerhuur.Repository;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace LPSiminBotenVerhuur.Forms
{
    /// <summary>
    /// The form used to calculate action radius.
    /// </summary>
    public partial class BerekenActieRadiusMotorboot : Form
    {
        /// <summary>
        /// The repository used for products.
        /// </summary>
        private ArtikelRepository artikelRepo = new ArtikelRepository();

        /// <summary>
        /// Initializes a new instance of the <see cref="BerekenActieRadiusMotorboot"/> class.
        /// </summary>
        public BerekenActieRadiusMotorboot()
        {
            InitializeComponent();
            comboBoxMotorboten.DataSource = artikelRepo.GetAllMotorboten();
        }

        /// <summary>
        /// Button action for calculate button.
        /// </summary>
        /// <param name="sender">The parameter is not used.</param>
        /// <param name="e">The parameter is not used.</param>
        private void BtnBereken_Click(object sender, EventArgs e)
        {
            Boot boot = (Boot)comboBoxMotorboten.SelectedItem;
            lblActieRadius.Text = "Actie radius: " + boot.GetActieRadius();
        }

        /// <summary>
        /// Action on load of form.
        /// </summary>
        /// <param name="sender">The parameter is not used.</param>
        /// <param name="e">The parameter is not used.</param>
        private void BerekenActieRadiusMotorboot_Load(object sender, EventArgs e)
        {
            CenterToScreen();
        }
    }
}
