﻿using LPSiminBotenVerhuur.Model;
using LPSiminBotenVerhuur.Repository;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace LPSiminBotenVerhuur.Forms
{
    /// <summary>
    /// Form used for calculating amount of waters.
    /// </summary>
    public partial class BerekenFrieseMerenForm : Form
    {
        /// <summary>
        /// Repository used for products.
        /// </summary>
        private ArtikelRepository artikelRepo = new ArtikelRepository();

        /// <summary>
        /// Initializes a new instance of the <see cref="BerekenFrieseMerenForm"/> class.
        /// </summary>
        public BerekenFrieseMerenForm()
        {
            InitializeComponent();
            SetupArtikelenComboBox();
        }

        /// <summary>
        /// Setup products combo box.
        /// </summary>
        private void SetupArtikelenComboBox()
        {
            comboBoxArtikelen.DataSource = artikelRepo.GetAll();
        }

        /// <summary>
        /// Action when clicked on calculate button.
        /// </summary>
        /// <param name="sender">The parameter is not used.</param>
        /// <param name="e">The parameter is not used.</param>
        private void BtnBereken_Click(object sender, EventArgs e)
        {
            decimal budget = nudBudget.Value;
            bool noordzee = checkNoordzee.Checked;
            bool ijsselmeer = checkIjsselmeer.Checked;

            if (noordzee)
            {
                budget -= 2;
            }

            if (ijsselmeer)
            {
                budget -= 2;
            }

            int bootDatGeenKanoIsCount = 0;
            foreach (ArtikelMetAantal artikelMetAantal in listBoxProducten.Items)
            {
                if (artikelMetAantal.Artikel is Boot)
                {
                    Boot boot = (Boot)artikelMetAantal.Artikel;
                    switch (boot.Type)
                    {
                        case BootType.ZeilbootLaser:
                        case BootType.ZeilbootValk:
                        case BootType.Kano:
                            budget -= 10;
                            bootDatGeenKanoIsCount++;
                            break;
                        case BootType.MotorbootKruiser:
                            budget -= 15;
                            break;
                    }
                }
                else if (artikelMetAantal.Artikel is BijkomendArtikel)
                {
                    budget -= 1.25m;
                }
            }

            if (budget <= 0)
            {
                MessageBox.Show("Budget is te laag.");
                return;
            }

            if (budget > 5)
            {
                budget -= bootDatGeenKanoIsCount * 0.5m;
            }

            int aantalMeren = (int)Math.Floor(budget);
            MessageBox.Show("U kunt " + aantalMeren + " meren bevaren voor uw budget.");
        }

        /// <summary>
        /// Gets called when add button is clicked.
        /// </summary>
        /// <param name="sender">The parameter is not used.</param>
        /// <param name="e">The parameter is not used.</param>
        private void BtnToevoegen_Click(object sender, EventArgs e)
        {
            if (comboBoxArtikelen.SelectedItem == null)
            {
                return;
            }

            foreach (ArtikelMetAantal artikel in listBoxProducten.Items)
            {
                if (artikel.Artikel == comboBoxArtikelen.SelectedItem)
                {
                    int index = listBoxProducten.Items.IndexOf(artikel);
                    ((ArtikelMetAantal)listBoxProducten.Items[index]).Aantal++;
                    RefreshList(listBoxProducten);
                    return;
                }
            }

            listBoxProducten.Items.Add(new ArtikelMetAantal((Artikel)comboBoxArtikelen.SelectedItem, 1));
        }

        /// <summary>
        /// Refresh a list.
        /// </summary>
        /// <param name="lb">The list to refresh.</param>
        private void RefreshList(ListBox lb)
        {
            lb.DisplayMember = string.Empty;
            lb.DisplayMember = "Refresh";
        }

        /// <summary>
        /// Action on load of form.
        /// </summary>
        /// <param name="sender">The parameter is not used.</param>
        /// <param name="e">The parameter is not used.</param>
        private void BerekenFrieseMerenForm_Load(object sender, EventArgs e)
        {
            CenterToScreen();
        }
    }
}
