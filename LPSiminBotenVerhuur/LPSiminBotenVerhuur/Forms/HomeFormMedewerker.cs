﻿using LPSiminBotenVerhuur.Forms;
using LPSiminBotenVerhuur.Model;
using System.Windows.Forms;

namespace LPSiminBotenVerhuur
{
    /// <summary>
    /// The main form of the application. 
    /// </summary>
    public partial class HomeFormMedewerker : Form
    {
        /// <summary>
        /// The account that is currently logged in.
        /// </summary>
        private Account loggedInAccount;

        /// <summary>
        /// Initializes a new instance of the <see cref="HomeFormMedewerker"/> class.
        /// </summary>
        /// <param name="loggedInAccount">The account that is currently logged in.</param>
        public HomeFormMedewerker(Account loggedInAccount)
        {
            InitializeComponent();
            this.loggedInAccount = loggedInAccount;
            lbWelkom.Text = string.Format("Welkom {0} {1}!", loggedInAccount.AccountRol.ToString(), loggedInAccount.Naam);
        }

        /// <summary>
        /// Action after the form has loaded.
        /// </summary>
        /// <param name="sender">The parameter is not used.</param>
        /// <param name="e">The parameter is not used.</param>
        private void MainForm_Load(object sender, System.EventArgs e)
        {
            CenterToScreen();
        }

        /// <summary>
        /// Button click action.
        /// </summary>
        /// <param name="sender">The parameter is not used.</param>
        /// <param name="e">The parameter is not used.</param>
        private void BtnHuurderToevoegen_Click(object sender, System.EventArgs e)
        {
            new ToevoegenHuurderForm().Show();
        }

        /// <summary>
        /// Button click action.
        /// </summary>
        /// <param name="sender">The parameter is not used.</param>
        /// <param name="e">The parameter is not used.</param>
        private void BtnHuurcontractOpstellen_Click(object sender, System.EventArgs e)
        {
            new HuurcontractOpstellenForm(loggedInAccount).Show();
        }

        /// <summary>
        /// Action on closing form.
        /// </summary>
        /// <param name="sender">The parameter is not used.</param>
        /// <param name="e">The parameter is not used.</param>
        private void HomeFormMedewerker_FormClosing(object sender, FormClosingEventArgs e)
        {
            Application.Exit();
        }

        /// <summary>
        /// Action on show rent contracts button.
        /// </summary>
        /// <param name="sender">The parameter is not used.</param>
        /// <param name="e">The parameter is not used.</param>
        private void BtnHuurcontractenBekijken_Click(object sender, System.EventArgs e)
        {
            new HuurcontractenBekijkenForm().Show();
        }

        /// <summary>
        /// Button action.
        /// </summary>
        /// <param name="sender">The parameter is not used.</param>
        /// <param name="e">The parameter is not used.</param>
        private void BtnBerekenAantalFrieseMeren_Click(object sender, System.EventArgs e)
        {
            new BerekenFrieseMerenForm().Show();
        }

        /// <summary>
        /// Button action.
        /// </summary>
        /// <param name="sender">The parameter is not used.</param>
        /// <param name="e">The parameter is not used.</param>
        private void BtnActieRadiusBerekenen_Click(object sender, System.EventArgs e)
        {
            new BerekenActieRadiusMotorboot().Show();
        }

        /// <summary>
        /// Button action.
        /// </summary>
        /// <param name="sender">The parameter is not used.</param>
        /// <param name="e">The parameter is not used.</param>
        private void BtnGevoelsTemperatuur_Click(object sender, System.EventArgs e)
        {
            new GevoelstemperatuurForm().Show();
        }
    }
}
