﻿using LPSiminBotenVerhuur.Model;
using LPSiminBotenVerhuur.Repository;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace LPSiminBotenVerhuur.Forms
{
    /// <summary>
    /// A rent contract is set up in this form.
    /// </summary>
    public partial class HuurcontractOpstellenForm : Form
    {
        /// <summary>
        /// The form used to add a contract.
        /// </summary>
        private Account loggedInAccount;

        /// <summary>
        /// The repository used for products.
        /// </summary>
        private ArtikelRepository artikelRepo;

        /// <summary>
        /// List of products.
        /// </summary>
        private List<ArtikelMetAantal> artikelen;

        /// <summary>
        /// Initializes a new instance of the <see cref="HuurcontractOpstellenForm"/> class.
        /// </summary>
        /// <param name="loggedInAccount">Logged in account.</param>
        public HuurcontractOpstellenForm(Account loggedInAccount)
        {
            InitializeComponent();
            this.loggedInAccount = loggedInAccount;
            artikelRepo = new ArtikelRepository();
            artikelen = new List<ArtikelMetAantal>();
        }

        /// <summary>
        /// Action after the form is loaded.
        /// </summary>
        /// <param name="sender">The parameter is not used.</param>
        /// <param name="e">The parameter is not used.</param>
        private void HuurcontractOpstellenForm_Load(object sender, EventArgs e)
        {
            lbNaamVerhuurder.Text = "Verhuurder: " + loggedInAccount.Naam;
            SetupComboBoxAssortiment();
            SetupComboBoxHuurders();
            SetupDataTableProducten();
            CenterToScreen();
        }

        /// <summary>
        /// Sets up the products table.
        /// </summary>
        private void SetupDataTableProducten()
        {
            dataGridViewArtikelen.Columns.Add("naam", "Naam");
            dataGridViewArtikelen.Columns.Add("prijs", "Prijs");
            dataGridViewArtikelen.Columns.Add("aantal", "Aantal");
        }

        /// <summary>
        /// Sets up the combo box.
        /// </summary>
        private void SetupComboBoxAssortiment()
        {
            List<Boot> boten = artikelRepo.GetAllBoten();
            List<BijkomendArtikel> bijkomendeArtikels = artikelRepo.GetAllBijkomendeArtikels();
            comboBoxAssortiment.Items.AddRange(boten.ToArray());
            comboBoxAssortiment.Items.AddRange(bijkomendeArtikels.ToArray());
        }

        /// <summary>
        /// Sets up the combo box for hire.
        /// </summary>
        private void SetupComboBoxHuurders()
        {
            HuurderRepository huurderRepo = new HuurderRepository();
            List<Huurder> huurders = huurderRepo.GetAll();
            comboBoxHuurders.Items.AddRange(huurders.ToArray());
        }

        /// <summary>
        /// Button action.
        /// </summary>
        /// <param name="sender">The parameter is not used.</param>
        /// <param name="e">The parameter is not used.</param>
        private void BtnProductToevoegen_Click(object sender, EventArgs e)
        {
            if (comboBoxAssortiment.SelectedItem == null)
            {
                MessageBox.Show("Je hebt geen artikel geselecteerd!");
                return;
            }

            Artikel geselecteerdArtikel = (Artikel)comboBoxAssortiment.SelectedItem;

            bool duplicate = false;
            foreach (ArtikelMetAantal artikelMetAantal in artikelen)
            {
                if (artikelMetAantal.Artikel == geselecteerdArtikel)
                {
                    artikelMetAantal.Aantal += 1;
                    duplicate = true;
                }
            }

            if (!duplicate)
            {
                artikelen.Add(new ArtikelMetAantal(geselecteerdArtikel, 1));
            }

            AddProductToGridViewProducten(geselecteerdArtikel);
        }

        /// <summary>
        /// Adds products to grid view.
        /// </summary>
        /// <param name="artikel">Product to add.</param>
        private void AddProductToGridViewProducten(Artikel artikel)
        {
            for (int i = 0; i < dataGridViewArtikelen.Rows.Count; i++)
            {
                DataGridViewRow row = dataGridViewArtikelen.Rows[i];
                if ((string)row.Cells["naam"].Value == artikel.Naam)
                {
                    int aantal = Convert.ToInt32(row.Cells["aantal"].Value);
                    row.Cells["aantal"].Value = aantal + 1;
                    return;
                }
            }

            dataGridViewArtikelen.Rows.Add(artikel.Naam, artikel.Prijs, 1);
        }

        /// <summary>
        /// Button action.
        /// </summary>
        /// <param name="sender">The parameter is not used.</param>
        /// <param name="e">The parameter is not used.</param>
        private void BtnBevestig_Click(object sender, EventArgs e)
        {
            if (comboBoxHuurders.SelectedItem == null)
            {
                MessageBox.Show("Je hebt geen huurder geselecteerd!");
                return;
            }

            if (dataGridViewArtikelen.RowCount == 0)
            {
                MessageBox.Show("De producten lijst is leeg!");
                return;
            }

            string verhuurNaam = loggedInAccount.Naam;
            Huurder huurder = (Huurder)comboBoxHuurders.SelectedItem;
            DateTime startDatum = dateTimePickerStart.Value;
            DateTime eindDatum = dateTimePickerEinde.Value;
            HuurcontractRepository huurcontractRepo = new HuurcontractRepository();
            Huurcontract huurcontract = new Huurcontract(loggedInAccount.Naam, huurder, artikelen, startDatum, eindDatum);
            if (huurcontractRepo.InsertHuurcontract(huurcontract))
            {
                MessageBox.Show("Huur contract is aangemaakt!");
                Close();
            }
            else
            {
                MessageBox.Show("Er is iets fout gegaan!");
            }
        }
    }
}
