﻿using LPSiminBotenVerhuur.Model;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace LPSiminBotenVerhuur.Forms
{
    /// <summary>
    /// Form used to calculate temperature.
    /// </summary>
    public partial class GevoelstemperatuurForm : Form
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="GevoelstemperatuurForm"/> class.
        /// </summary>
        public GevoelstemperatuurForm()
        {
            InitializeComponent();
        }

        /// <summary>
        /// Calculate button.
        /// </summary>
        /// <param name="sender">The parameter is not used.</param>
        /// <param name="e">The parameter is not used.</param>
        private void BtnBereken_Click(object sender, EventArgs e)
        {
            List<GevoelsTemperatuur> temps = new List<GevoelsTemperatuur>();
            DialogResult result = openFileDialogTemperatuur.ShowDialog();
            if (result == DialogResult.OK)
            {
                string path = openFileDialogTemperatuur.FileName;
                StreamReader file = new StreamReader(path);
                string line;
                while ((line = file.ReadLine()) != null)
                {
                    string[] array = ParseLine(line);
                    int gevoelstemperatuur = BerekenGevoelstemperatuur(Convert.ToInt32(array[1]), Convert.ToInt32(array[2]));

                    DateTime dag;
                    DateTime.TryParse(array[0].ToString(), out dag);
                    temps.Add(new GevoelsTemperatuur(dag, gevoelstemperatuur));
                }
            }

            DateTime start = dtpStartDatum.Value;
            DateTime eind = dtpEindDatum.Value;

            listBoxResultaat.Items.Clear();

            for (DateTime date = start; date.Date <= eind.Date; date = date.AddDays(1))
            {
                string resultaat = date.ToString("dd/MM/yyyy") + ": Geen gegevens beschikbaar";
                foreach (GevoelsTemperatuur temp in temps)
                {
                    if (temp.Dag.Date == date.Date)
                    {
                        resultaat = date.ToString("dd/MM/yyyy") + ": " + temp.Temperatuur + " °C";
                    }
                }

                listBoxResultaat.Items.Add(resultaat);
            }
        }

        /// <summary>
        /// Parses a line.
        /// </summary>
        /// <param name="line">Line to parse.</param>
        /// <returns>String array.</returns>
        private string[] ParseLine(string line)
        {
            return line.Split(';');
        }

        /// <summary>
        /// Calculates the temperature.
        /// </summary>
        /// <param name="buitenTemperatuur">Outside temperature.</param>
        /// <param name="windsnelheid">Wind speed.</param>
        /// <returns>The temperature.</returns>
        private int BerekenGevoelstemperatuur(int buitenTemperatuur, int windsnelheid)
        {
            int lucht = buitenTemperatuur;
            int v = windsnelheid;
            return Convert.ToInt32(33 + (lucht - 33) * (0.474 + 0.454 * Math.Sqrt(v) - 0.0454 * v));
        }

        /// <summary>
        /// Action on load.
        /// </summary>
        /// <param name="sender">The parameter is not used.</param>
        /// <param name="e">The parameter is not used.</param>
        private void GevoelstemperatuurForm_Load(object sender, EventArgs e)
        {
            CenterToScreen();
        }
    }
}
