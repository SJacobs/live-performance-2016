﻿using LPSiminBotenVerhuur.Model;
using LPSiminBotenVerhuur.Repository.Account;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace LPSiminBotenVerhuur.Forms
{
    /// <summary>
    /// Login form.
    /// </summary>
    public partial class LoginForm : Form
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="LoginForm"/> class.
        /// </summary>
        public LoginForm()
        {
            InitializeComponent();
        }

        /// <summary>
        /// Action when login button is clicked.
        /// </summary>
        /// <param name="sender">The parameter is not used.</param>
        /// <param name="e">The parameter is not used.</param>
        private void BtnLogin_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrWhiteSpace(tbNaam.Text) || string.IsNullOrWhiteSpace(tbWachtwoord.Text))
            {
                MessageBox.Show("Vul alle velden in.");
                return;
            }

            AccountRepository accountRepo = new AccountRepository();
            if (!accountRepo.IsAccountCorrect(tbNaam.Text, tbWachtwoord.Text))
            {
                MessageBox.Show("Naam of wachtwoord is verkeerd.");
                return;
            }

            AccountRol rol = accountRepo.GetAccountRol(tbNaam.Text);
            Account loggedInAccount = new Account(tbNaam.Text, tbWachtwoord.Text, rol);
            new HomeFormMedewerker(loggedInAccount).Show();
            Close();
        }

        /// <summary>
        /// Action when form is loaded.
        /// </summary>
        /// <param name="sender">The parameter is not used.</param>
        /// <param name="e">The parameter is not used.</param>
        private void LoginForm_Load(object sender, EventArgs e)
        {
            CenterToScreen();
        }
    }
}
