﻿using LPSiminBotenVerhuur.Repository;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace LPSiminBotenVerhuur.Forms
{
    /// <summary>
    /// Add a renter on this form.
    /// </summary>
    public partial class ToevoegenHuurderForm : Form
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="ToevoegenHuurderForm"/> class.
        /// </summary>
        public ToevoegenHuurderForm()
        {
            InitializeComponent();
        }

        /// <summary>
        /// Action when form is loaded.
        /// </summary>
        /// <param name="sender">The parameter is not used.</param>
        /// <param name="e">The parameter is not used.</param>
        private void ToevoegenHuurderForm_Load(object sender, EventArgs e)
        {
            CenterToScreen();
        }

        /// <summary>
        /// Action when add renter button is clicked.
        /// </summary>
        /// <param name="sender">The parameter is not used.</param>
        /// <param name="e">The parameter is not used.</param>
        private void BtnHuurderToevoegen_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrWhiteSpace(tbNaam.Text) || string.IsNullOrWhiteSpace(tbEmail.Text))
            {
                MessageBox.Show("Vul alle velden in.");
            }

            HuurderRepository huurderRepository = new HuurderRepository();
            if (huurderRepository.InsertHuurder(tbNaam.Text, tbEmail.Text))
            {
                MessageBox.Show("Huurder toegevoegd!");
                Close();
            }
            else
            {
                MessageBox.Show("Niet gelukt!");
            }
        }
    }
}
