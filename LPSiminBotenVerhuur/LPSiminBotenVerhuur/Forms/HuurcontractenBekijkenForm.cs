﻿using LPSiminBotenVerhuur.Model;
using LPSiminBotenVerhuur.Repository;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace LPSiminBotenVerhuur.Forms
{
    /// <summary>
    /// Form used to see rent contracts.
    /// </summary>
    public partial class HuurcontractenBekijkenForm : Form
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="HuurcontractenBekijkenForm"/> class.
        /// </summary>
        public HuurcontractenBekijkenForm()
        {
            InitializeComponent();
            SetupHuurcontractenList();
        }

        /// <summary>
        /// Sets up the list of contracts.
        /// </summary>
        private void SetupHuurcontractenList()
        {
            HuurcontractRepository huurcontractRepo = new HuurcontractRepository();
            listBoxHuurcontracten.DataSource = huurcontractRepo.GetAll();
        }

        /// <summary>
        /// Action on index changed.
        /// </summary>
        /// <param name="sender">The parameter is not used.</param>
        /// <param name="e">The parameter is not used.</param>
        private void ListBoxHuurcontracten_SelectedIndexChanged(object sender, EventArgs e)
        {
            Huurcontract contract = (Huurcontract)listBoxHuurcontracten.SelectedItem;
            lblVerhuurderNaam.Text = "Verhuurder naam: " + contract.VerhuurderNaam;
            lblDatumStart.Text = "Start datum: " + contract.DatumStart;
            lblEindDatum.Text = "Eind datum: " + contract.DatumEinde;
            ArtikelRepository artikelRepo = new ArtikelRepository();
            listBoxHuurcontractProducten.DataSource = artikelRepo.GetAllArtikelenForHuurcontractId(contract.Id);
        }

        /// <summary>
        /// Action on button click.
        /// </summary>
        /// <param name="sender">The parameter is not used.</param>
        /// <param name="e">The parameter is not used.</param>
        private void BtnExportTxt_Click(object sender, EventArgs e)
        {
            Huurcontract contract = (Huurcontract)listBoxHuurcontracten.SelectedItem;
            contract.ExportToTxt();
        }

        /// <summary>
        /// Action on load.
        /// </summary>
        /// <param name="sender">The parameter is not used.</param>
        /// <param name="e">The parameter is not used.</param>
        private void HuurcontractenBekijkenForm_Load(object sender, EventArgs e)
        {
            CenterToScreen();
        }
    }
}
