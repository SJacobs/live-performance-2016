﻿using LPSiminBotenVerhuur.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LPSiminBotenVerhuur.Repository.Account
{
    /// <summary>
    /// The account context interface.
    /// </summary>
    public interface IAccountContext
    {
        /// <summary>
        /// Checks whether the account is correct.
        /// </summary>
        /// <param name="naam">Name of account.</param>
        /// <param name="wachtwoord">Password of account.</param>
        /// <returns>Whether the account is correct.</returns>
        bool IsAccountCorrect(string naam, string wachtwoord);

        /// <summary>
        /// Gets the account role.
        /// </summary>
        /// <param name="naam">Name of account.</param>
        /// <returns>The account role.</returns>
        AccountRol GetAccountRol(string naam);
    }
}
