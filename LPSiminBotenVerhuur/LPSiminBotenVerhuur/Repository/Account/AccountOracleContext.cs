﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LPSiminBotenVerhuur.Model;
using Oracle.ManagedDataAccess.Client;
using System.Data;

namespace LPSiminBotenVerhuur.Repository.Account
{
    /// <summary>
    /// Communicates with the oracle database.
    /// </summary>
    public class AccountOracleContext : IAccountContext
    {
        /// <summary>
        /// The database manager used for queries.
        /// </summary>
        private DatabaseManager databaseManager;

        /// <summary>
        /// Initializes a new instance of the <see cref="AccountOracleContext"/> class.
        /// </summary>
        public AccountOracleContext()
        {
            databaseManager = new DatabaseManager();
        }

        /// <summary>
        /// Gets the account role.
        /// </summary>
        /// <param name="naam">Name of account.</param>
        /// <returns>Role of account.</returns>
        public AccountRol GetAccountRol(string naam)
        {
            OracleCommand command = new OracleCommand();
            command.CommandText = "SELECT rol FROM Account WHERE naam = :naam";
            command.Parameters.Add("naam", naam);

            DataTable dt = databaseManager.ReadQuery(command);
            string rolString = dt.Rows[0]["rol"].ToString();
            AccountRol accountRol;
            Enum.TryParse(rolString, out accountRol);
            return accountRol;
        }

        /// <summary>
        /// Checks whether the account is correct.
        /// </summary>
        /// <param name="naam">Name of account.</param>
        /// <param name="wachtwoord">Password of account.</param>
        /// <returns>Returns whether the account is correct.</returns>
        public bool IsAccountCorrect(string naam, string wachtwoord)
        {
            OracleCommand command = new OracleCommand();
            command.CommandText = "SELECT * FROM Account WHERE naam = :naam AND wachtwoord = :wachtwoord";
            command.Parameters.Add("naam", naam);
            command.Parameters.Add("wachtwoord", wachtwoord);

            DataTable dt = databaseManager.ReadQuery(command);
            return dt.Rows.Count >= 1;
        }
    }
}
