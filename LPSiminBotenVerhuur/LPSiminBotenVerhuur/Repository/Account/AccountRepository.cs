﻿using LPSiminBotenVerhuur.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LPSiminBotenVerhuur.Repository.Account
{
    /// <summary>
    /// Communicates with the database context to get information of accounts.
    /// </summary>
    public class AccountRepository
    {
        /// <summary>
        /// The context used.
        /// </summary>
        private IAccountContext context;

        /// <summary>
        /// Initializes a new instance of the <see cref="AccountRepository"/> class.
        /// </summary>
        public AccountRepository()
        {
            context = new AccountOracleContext();
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="AccountRepository"/> class.
        /// </summary>
        /// <param name="context">The context to use.</param>
        public AccountRepository(IAccountContext context)
        {
            this.context = context;
        }

        /// <summary>
        /// Checks whether the account is correct.
        /// </summary>
        /// <param name="naam">Name of account.</param>
        /// <param name="wachtwoord">Password of account.</param>
        /// <returns>Whether the account is correct.</returns>
        public bool IsAccountCorrect(string naam, string wachtwoord)
        {
            return context.IsAccountCorrect(naam, wachtwoord);
        }

        /// <summary>
        /// Gets the role of the account.
        /// </summary>
        /// <param name="naam">Name of account.</param>
        /// <returns>The account role.</returns>
        public AccountRol GetAccountRol(string naam)
        {
            return context.GetAccountRol(naam);
        }
    }
}
