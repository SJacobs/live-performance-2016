﻿using LPSiminBotenVerhuur.Model;
using Oracle.ManagedDataAccess.Client;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LPSiminBotenVerhuur.Repository
{
    /// <summary>
    /// The oracle context for renter.
    /// </summary>
    public class HuurderOracleContext : IHuurderContext
    {
        /// <summary>
        /// The database manager used to execute queries.
        /// </summary>
        private DatabaseManager databaseManager;

        /// <summary>
        /// Initializes a new instance of the <see cref="HuurderOracleContext"/> class.
        /// </summary>
        public HuurderOracleContext()
        {
            databaseManager = new DatabaseManager();
        }

        /// <summary>
        /// Insert a renter.
        /// </summary>
        /// <param name="naam">Name of renter.</param>
        /// <param name="email">Email of renter.</param>
        /// <returns>Whether the query was successful.</returns>
        public bool InsertHuurder(string naam, string email)
        {
            OracleCommand oracleCommand = new OracleCommand();
            oracleCommand.CommandText = "INSERT INTO Huurder(naam, email)VALUES(:naam, :email)";
            oracleCommand.Parameters.Add("naam", naam);
            oracleCommand.Parameters.Add("email", email);
            return databaseManager.ExecuteQuery(oracleCommand);
        }

        public List<Huurder> GetAll()
        {
            string query = "SELECT * FROM Huurder";
            DataTable dt = databaseManager.ReadQuery(query);
            List<Huurder> huurders = new List<Huurder>();
            foreach (DataRow row in dt.Rows)
            {
                int id = Convert.ToInt32(row["id"]);
                string naam = row["naam"].ToString();
                string email = row["email"].ToString();
                huurders.Add(new Huurder(id, naam, email));
            }
            return huurders;
        }

        public Huurder GetById(int huurderId)
        {
            string query = "SELECT * FROM Huurder WHERE id = " + huurderId;
            DataTable dt = databaseManager.ReadQuery(query);

            if (dt.Rows.Count == 0)
            {
                return null;
            }

            DataRow row = dt.Rows[0];

            Huurder huurder = new Huurder(row["naam"].ToString(), row["email"].ToString());
            return huurder;
        }
    }
}
