﻿using LPSiminBotenVerhuur.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LPSiminBotenVerhuur.Repository
{
    /// <summary>
    /// The renter context interface.
    /// </summary>
    public interface IHuurderContext
    {
        /// <summary>
        /// Insert a renter.
        /// </summary>
        /// <param name="naam">Name of renter.</param>
        /// <param name="email">Email of renter.</param>
        /// <returns>Whether the query was successful.</returns>
        bool InsertHuurder(string naam, string email);

        List<Huurder> GetAll();

        Huurder GetById(int huurderId);
    }
}
