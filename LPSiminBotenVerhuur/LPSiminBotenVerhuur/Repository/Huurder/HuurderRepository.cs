﻿using LPSiminBotenVerhuur.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LPSiminBotenVerhuur.Repository
{
    /// <summary>
    /// This class communicates with the context to get information about the renter.
    /// </summary>
    public class HuurderRepository
    {
        /// <summary>
        /// The context used.
        /// </summary>
        private IHuurderContext context;

        /// <summary>
        /// Initializes a new instance of the <see cref="HuurderRepository"/> class.
        /// </summary>
        public HuurderRepository()
        {
            context = new HuurderOracleContext();
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="HuurderRepository"/> class.
        /// </summary>
        /// <param name="context">Context to use.</param>
        public HuurderRepository(IHuurderContext context)
        {
            this.context = context;
        }

        /// <summary>
        /// Insert a renter.
        /// </summary>
        /// <param name="naam">Name of renter.</param>
        /// <param name="email">Email of renter.</param>
        /// <returns>Whether the query was successful.</returns>
        public bool InsertHuurder(string naam, string email)
        {
            return context.InsertHuurder(naam, email);
        }

        public List<Huurder> GetAll()
        {
            return context.GetAll();
        }

        public Huurder GetById(int huurderId)
        {
            return context.GetById(huurderId);
        }
    }
}
