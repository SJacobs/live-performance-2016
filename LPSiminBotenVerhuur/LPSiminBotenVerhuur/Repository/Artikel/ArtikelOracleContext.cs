﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LPSiminBotenVerhuur.Model;
using System.Data;
using Oracle.ManagedDataAccess.Client;

namespace LPSiminBotenVerhuur.Repository
{
    /// <summary>
    /// Represents the oracle database context.
    /// </summary>
    public class ArtikelOracleContext : IArtikelContext
    {
        /// <summary>
        /// Used to execute queries.
        /// </summary>
        private DatabaseManager databaseManager;

        /// <summary>
        /// Initializes a new instance of the <see cref="ArtikelOracleContext"/> class.
        /// </summary>
        public ArtikelOracleContext()
        {
            databaseManager = new DatabaseManager();
        }

        /// <summary>
        /// Gets all extra products.
        /// </summary>
        /// <returns>All articles.</returns>
        public List<BijkomendArtikel> GetAllBijkomendeArtikels()
        {
            string query = "SELECT * FROM Artikel a, BijkomendArtikel b WHERE a.id = b.Artikel_id";
            DataTable dt = databaseManager.ReadQuery(query);

            List<BijkomendArtikel> bijkomendeArtikelen = new List<BijkomendArtikel>();
            foreach (DataRow row in dt.Rows)
            {
                int id = Convert.ToInt32(row["id"]);
                string naam = row["naam"].ToString();
                decimal prijs = Convert.ToDecimal(row["prijs"]);
                bijkomendeArtikelen.Add(new BijkomendArtikel(id, naam, prijs));
            }

            return bijkomendeArtikelen;
        }

        /// <summary>
        /// Gets all extra products for rent contract id.
        /// </summary>
        /// <param name="huurcontractId">The id of the rent contract.</param>
        /// <returns>List with products.</returns>
        public List<ArtikelMetAantal> GetAllBijkomendeArtikelenForHuurcontract(int huurcontractId)
        {
            string query = string.Format("SELECT b.id, a.naam, a.prijs, ha.aantal FROM Artikel a, BijkomendArtikel b, Huurcontract_Artikel ha WHERE a.id = b.artikel_id AND ha.Huurcontract_id = {0} AND b.Artikel_id = ha.Artikel_id", huurcontractId);
            DataTable dt = databaseManager.ReadQuery(query);

            List<ArtikelMetAantal> artikelen = new List<ArtikelMetAantal>();
            foreach (DataRow row in dt.Rows)
            {
                int aantal = Convert.ToInt32(row["aantal"]);
                int id = Convert.ToInt32(row["id"]);
                string naam = row["naam"].ToString();
                decimal prijs = Convert.ToDecimal(row["prijs"]);
                artikelen.Add(new ArtikelMetAantal(new BijkomendArtikel(id, naam, prijs), aantal));
            }

            return artikelen;
        }

        /// <summary>
        /// Gets all boats for rent contract id.
        /// </summary>
        /// <param name="huurcontractId">The id of the rent contract.</param>
        /// <returns>List with products and amounts.</returns>
        public List<ArtikelMetAantal> GetAllBotenForHuurcontract(int huurcontractId)
        {
            string query = string.Format("SELECT b.id, a.naam, a.prijs, b.type, b.tankinhoud, ha.aantal FROM Artikel a, Boot b, Huurcontract_Artikel ha WHERE a.id = b.artikel_id AND ha.Huurcontract_id = {0} AND b.Artikel_id = ha.Artikel_id", huurcontractId);
            DataTable dt = databaseManager.ReadQuery(query);

            List<ArtikelMetAantal> artikelen = new List<ArtikelMetAantal>();
            foreach (DataRow row in dt.Rows)
            {
                int aantal = Convert.ToInt32(row["aantal"]);
                int id = Convert.ToInt32(row["id"]);
                string naam = row["naam"].ToString();
                decimal prijs = Convert.ToDecimal(row["prijs"]);
                BootType bootType = (BootType)Enum.Parse(typeof(BootType), row["type"].ToString());
                int tankInhoud = Convert.ToInt32(row["tankInhoud"]);
                artikelen.Add(new ArtikelMetAantal(new Boot(id, naam, prijs, bootType, tankInhoud), aantal));
            }

            return artikelen;
        }

        /// <summary>
        /// Gets all products for rent contract id.
        /// </summary>
        /// <param name="huurcontractId">Id of rent contract.</param>
        /// <returns>The list with products.</returns>
        public List<ArtikelMetAantal> GetAllArtikelenForHuurcontractId(int huurcontractId)
        {
            List<ArtikelMetAantal> artikelen = new List<ArtikelMetAantal>();
            artikelen.AddRange(GetAllBotenForHuurcontract(huurcontractId));
            artikelen.AddRange(GetAllBijkomendeArtikelenForHuurcontract(huurcontractId));
            return artikelen;
        }

        /// <summary>
        /// Gets all products.
        /// </summary>
        /// <returns>List with all products.</returns>
        public List<Artikel> GetAll()
        {
            List<Artikel> artikelen = new List<Artikel>();
            artikelen.AddRange(GetAllBoten());
            artikelen.AddRange(GetAllBijkomendeArtikels());
            return artikelen;
        }

        /// <summary>
        /// Gets all boats.
        /// </summary>
        /// <returns>All boats.</returns>
        public List<Boot> GetAllBoten()
        {
            string query = "SELECT * FROM Artikel a, Boot b WHERE a.id = b.Artikel_id";
            DataTable dt = databaseManager.ReadQuery(query);

            List<Boot> boten = new List<Boot>();
            foreach (DataRow row in dt.Rows)
            {
                int id = Convert.ToInt32(row["id"]);
                string naam = row["naam"].ToString();
                decimal prijs = Convert.ToDecimal(row["prijs"]);
                BootType bootType = (BootType)Enum.Parse(typeof(BootType), row["type"].ToString());
                int tankInhoud = Convert.ToInt32(row["tankInhoud"]);
                boten.Add(new Boot(id, naam, prijs, bootType, tankInhoud));
            }

            return boten;
        }
    }
}
