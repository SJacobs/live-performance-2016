﻿using LPSiminBotenVerhuur.Model;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LPSiminBotenVerhuur.Repository
{
    /// <summary>
    /// Contains the required methods.
    /// </summary>
    public interface IArtikelContext
    {
        /// <summary>
        /// Gets all extra products.
        /// </summary>
        /// <returns>List containing all extra products.</returns>
        List<BijkomendArtikel> GetAllBijkomendeArtikels();

        /// <summary>
        /// Gets all boats.
        /// </summary>
        /// <returns>List containing all boats.</returns>
        List<Boot> GetAllBoten();

        /// <summary>
        /// Gets all extra products for rent contract.
        /// </summary>
        /// <param name="huurcontractId">Id of rent contract.</param>
        /// <returns>List of products.</returns>
        List<ArtikelMetAantal> GetAllBijkomendeArtikelenForHuurcontract(int huurcontractId);

        /// <summary>
        /// Gets all boats for rent contract.
        /// </summary>
        /// <param name="huurcontractId">Id of rent contract.</param>
        /// <returns>List with products.</returns>
        List<ArtikelMetAantal> GetAllBotenForHuurcontract(int huurcontractId);

        /// <summary>
        /// Gets all products for rent contract.
        /// </summary>
        /// <param name="huurcontractId">Id of rent contract.</param>
        /// <returns>List of products.</returns>
        List<ArtikelMetAantal> GetAllArtikelenForHuurcontractId(int huurcontractId);

        /// <summary>
        /// Gets all products.
        /// </summary>
        /// <returns>List with all products.</returns>
        List<Artikel> GetAll();
    }
}
