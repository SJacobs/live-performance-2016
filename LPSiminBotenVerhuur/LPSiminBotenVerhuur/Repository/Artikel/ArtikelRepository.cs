﻿using LPSiminBotenVerhuur.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LPSiminBotenVerhuur.Repository
{
    /// <summary>
    /// The repository gets the information from the database context.
    /// </summary>
    public class ArtikelRepository
    {
        /// <summary>
        /// The database context used.
        /// </summary>
        private IArtikelContext context;

        /// <summary>
        /// Initializes a new instance of the <see cref="ArtikelRepository"/> class.
        /// </summary>
        public ArtikelRepository()
        {
            context = new ArtikelOracleContext();
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="ArtikelRepository"/> class.
        /// </summary>
        /// <param name="context">The context to use.</param>
        public ArtikelRepository(IArtikelContext context)
        {
            this.context = context;
        }

        /// <summary>
        /// Gets all boats.
        /// </summary>
        /// <returns>List containing all boats.</returns>
        public List<Boot> GetAllBoten()
        {
            return context.GetAllBoten();
        }

        /// <summary>
        /// Gets all motorboats.
        /// </summary>
        /// <returns>List of motorboats.</returns>
        public List<Boot> GetAllMotorboten()
        {
            List<Boot> motorboten = new List<Boot>();
            foreach (Boot boot in GetAllBoten())
            {
                if (boot.Type == BootType.MotorbootKruiser)
                {
                    motorboten.Add(boot);
                }
            }

            return motorboten;
        }

        /// <summary>
        /// Gets all products for rent contract.
        /// </summary>
        /// <param name="huurcontractId">Id of rent contract.</param>
        /// <returns>List of products.</returns>
        public List<ArtikelMetAantal> GetAllArtikelenForHuurcontractId(int huurcontractId)
        {
            return context.GetAllArtikelenForHuurcontractId(huurcontractId);
        }

        /// <summary>
        /// Gets all extra products.
        /// </summary>
        /// <returns>List containing all products.</returns>
        public List<BijkomendArtikel> GetAllBijkomendeArtikels()
        {
            return context.GetAllBijkomendeArtikels();
        }

        /// <summary>
        /// Gets all products.
        /// </summary>
        /// <returns>List with all products.</returns>
        public List<Artikel> GetAll()
        {
            return context.GetAll();
        }
    }
}
