﻿using System;
using System.Collections.Generic;
using System.Data;
using Oracle.ManagedDataAccess.Client;

namespace LPSiminBotenVerhuur.Repository
{
    /// <summary>
    /// This class handles queries.
    /// </summary>
    public class DatabaseManager
    {
        /// <summary>
        /// The oracle connection used for executing queries.
        /// </summary>
        private string connectionString;

        /// <summary>
        /// Initializes a new instance of the <see cref="DatabaseManager"/> class.
        /// </summary>
        public DatabaseManager()
        {
            connectionString = "Data Source = (DESCRIPTION = (ADDRESS_LIST = (ADDRESS = (PROTOCOL = TCP)(HOST = fhictora01.fhict.local)(PORT = 1521)))(CONNECT_DATA = (SERVER = DEDICATED)(SERVICE_NAME = fhictora))); User ID = dbi344237; PASSWORD = simin123;";
        }

        /// <summary>
        /// Execute a query without parameters.
        /// </summary>
        /// <param name="query">The query to execute.</param>
        /// <returns>Whether the query was successful.</returns>
        public bool ExecuteQuery(string query)
        {
            return ExecuteQuery(new OracleCommand(query));
        }

        /// <summary>
        /// Execute a query.
        /// </summary>
        /// <param name="oracleCommand">The oracle command to execute.</param>
        /// <returns>Whether the query was successful.</returns>
        public bool ExecuteQuery(OracleCommand oracleCommand)
        {
            int result = -1;

            using (OracleConnection connection = new OracleConnection(connectionString))
            {
                connection.Open();
                oracleCommand.Connection = connection;

                using (oracleCommand)
                {
                    Console.WriteLine(oracleCommand.CommandText);
                    foreach (OracleParameter param in oracleCommand.Parameters)
                    {
                        Console.WriteLine(param.Value);
                    }

                    result = oracleCommand.ExecuteNonQuery();
                }
            }

            return result > -1;
        }

        /// <summary>
        /// Executes a ready query.
        /// </summary>
        /// <param name="query">The query to execute.</param>
        /// <returns>The data table with results.</returns>
        public DataTable ReadQuery(string query)
        {
            return ReadQuery(new OracleCommand(query));
        }

        /// <summary>
        /// Read query from database.
        /// </summary>
        /// <param name="command">Command to execute.</param>
        /// <returns>Data table of results.</returns>
        public DataTable ReadQuery(OracleCommand command)
        {
            DataTable dataTable = new DataTable();
            using (OracleConnection connection = new OracleConnection(connectionString))
            {
                connection.Open();
                using (command)
                {
                    command.Connection = connection;
                    using (OracleDataReader reader = command.ExecuteReader())
                    {
                        dataTable.Load(reader);
                    }
                }
            }

            return dataTable;
        }

        /// <summary>
        /// Gets a single result.
        /// </summary>
        /// <param name="query">The query to execute.</param>
        /// <returns>A single result.</returns>
        public int ExecuteScalar(string query)
        {
            using (OracleConnection connection = new OracleConnection(connectionString))
            {
                connection.Open();
                using (OracleCommand command = new OracleCommand(query, connection))
                {
                    object result = command.ExecuteScalar();
                    if (result == null)
                    {
                        return -1;
                    }

                    if (result == DBNull.Value)
                    {
                        return -1;
                    }

                    return Convert.ToInt32(result);
                }
            }
        }
    }
}
