﻿using LPSiminBotenVerhuur.Model;
using Oracle.ManagedDataAccess.Client;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LPSiminBotenVerhuur.Repository
{
    /// <summary>
    /// Oracle context for hire contract.
    /// </summary>
    public class HuurcontractOracleContext : IHuurcontractContext
    {
        /// <summary>
        /// Database manager used.
        /// </summary>
        private DatabaseManager databaseManager;

        /// <summary>
        /// Initializes a new instance of the <see cref="HuurcontractOracleContext"/> class.
        /// </summary>
        public HuurcontractOracleContext()
        {
            databaseManager = new DatabaseManager();
        }

        /// <summary>
        /// Insert a contract.
        /// </summary>
        /// <param name="huurcontract"></param>
        /// <returns>Whether contract inserted.</returns>
        public bool InsertHuurcontract(Huurcontract huurcontract)
        {
            OracleCommand command = new OracleCommand();
            command.CommandText = "INSERT INTO Huurcontract(Huurder_id, verhuurderNaam, datumStart, datumEinde)VALUES(:huurderId, :verhuurderNaam, :datumStart, :datumEinde)";
            command.Parameters.Add("huurderId", huurcontract.Huurder.Id);
            command.Parameters.Add("verhuurderNaam", huurcontract.VerhuurderNaam);
            command.Parameters.Add("datumStart", huurcontract.DatumStart);
            command.Parameters.Add("datumEinde", huurcontract.DatumEinde);

            if (!databaseManager.ExecuteQuery(command))
            {
                return false;
            }

            huurcontract.Id = GetLastHuurcontractId();

            foreach (ArtikelMetAantal artikelMetAantal in huurcontract.Artikelen)
            {
                if (artikelMetAantal.Artikel is Boot)
                {
                    if (!InsertHuurcontractBoot(huurcontract, (Boot)artikelMetAantal.Artikel, artikelMetAantal.Aantal))
                    {
                        return false;
                    }
                }

                if (artikelMetAantal.Artikel is BijkomendArtikel)
                {
                    if (!InsertHuurcontractBijkomendArtikel(huurcontract, (BijkomendArtikel)artikelMetAantal.Artikel, artikelMetAantal.Aantal))
                    {
                        return false;
                    }
                }
            }
            return true;
        }

        public bool InsertHuurcontractBijkomendArtikel(Huurcontract huurcontract, BijkomendArtikel bijkomendArtikel, int aantal)
        {
            DataTable dt = databaseManager.ReadQuery(string.Format("SELECT aantal, ba.artikel_id FROM Huurcontract_Artikel ha, BijkomendArtikel ba WHERE ha.Huurcontract_id = {0} AND ha.Artikel_id = ba.Artikel_id AND ba.id = {1}", huurcontract.Id, bijkomendArtikel.Id));

            if (dt.Rows.Count == 0)
            {
                OracleCommand command = new OracleCommand();
                command.CommandText = "INSERT INTO Huurcontract_Artikel(Huurcontract_id, Artikel_id, aantal)VALUES(:1, (SELECT artikel_id FROM BijkomendArtikel WHERE BijkomendArtikel_id = :2), :3)";
                command.Parameters.Add("1", huurcontract.Id);
                command.Parameters.Add("2", bijkomendArtikel.Id);
                command.Parameters.Add("3", aantal);
                return databaseManager.ExecuteQuery(command);
            }

            DataRow row = dt.Rows[0];

            int huidigAantal = Convert.ToInt32(row["aantal"]);
            int artikelId = Convert.ToInt32(row["artikel_id"]);

            return databaseManager.ExecuteQuery(string.Format("UPDATE Huurcontract_Artikel SET aantal = {0} WHERE Huurcontract_id = {1} AND Artikel_id = {2}", huidigAantal + aantal, huurcontract.Id, artikelId));

        }

        public bool InsertHuurcontractBoot(Huurcontract huurcontract, Boot boot, int aantal)
        {
            int artikelId = databaseManager.ExecuteScalar(string.Format("SELECT artikel_id FROM Boot WHERE id = {0}", boot.Id));
            if (artikelId == -1)
            {
                return false;
            }

            int huidigAantal = databaseManager.ExecuteScalar(string.Format("SELECT aantal FROM Huurcontract_Boot WHERE Huurcontract_id = {0} AND Boot_id = {1}", huurcontract.Id, boot.Id));

            if (huidigAantal == -1)
            {
                OracleCommand command = new OracleCommand();
                command.CommandText = "INSERT INTO Huurcontract_Artikel(Huurcontract_id, Artikel_id, aantal)VALUES(:1, :2, :3)";
                command.Parameters.Add("1", huurcontract.Id);
                command.Parameters.Add("2", artikelId);
                command.Parameters.Add("3", aantal);
                return databaseManager.ExecuteQuery(command);
            }

            return databaseManager.ExecuteQuery(string.Format("UPDATE Huurcontract_Artikel SET aantal = {0} WHERE Huurcontract_id = {1} AND Artikel_id = {2}", huidigAantal + aantal, huurcontract.Id, artikelId));
        }

        public int GetLastHuurcontractId()
        {
            return databaseManager.ExecuteScalar("SELECT MAX(id) FROM Huurcontract");
        }

        public int GetNextHuurcontractId()
        {
            int lastId = GetLastHuurcontractId();
            if (lastId == -1)
            {
                return 1;
            }
            return lastId + 1;
        }

        public List<Huurcontract> GetAll()
        {
            DataTable dt = databaseManager.ReadQuery("SELECT * FROM Huurcontract");
            List<Huurcontract> huurcontracten = new List<Huurcontract>();
            foreach (DataRow row in dt.Rows)
            {
                int id = Convert.ToInt32(row["id"]);
                int huurderId = Convert.ToInt32(row["Huurder_id"]);
                string verhuurNaam = row["verhuurderNaam"].ToString();
                HuurderRepository huurderRepo = new HuurderRepository();
                Huurder huurder = huurderRepo.GetById(huurderId);

                DateTime start;
                DateTime.TryParse(row["datumStart"].ToString(), out start);
                DateTime end;
                DateTime.TryParse(row["datumEinde"].ToString(), out end);

                ArtikelRepository artikelRepo = new ArtikelRepository();
                List<ArtikelMetAantal> artikelenMetAantal = artikelRepo.GetAllArtikelenForHuurcontractId(id);

                huurcontracten.Add(new Huurcontract(id, verhuurNaam, huurder, artikelenMetAantal, start, end));
            }
            return huurcontracten;
        }
    }
}
