﻿using LPSiminBotenVerhuur.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LPSiminBotenVerhuur.Repository
{
    public interface IHuurcontractContext
    {
        bool InsertHuurcontract(Huurcontract huurcontract);
        bool InsertHuurcontractBoot(Huurcontract huurcontract, Boot boot, int aantal);
        bool InsertHuurcontractBijkomendArtikel(Huurcontract huurcontract, BijkomendArtikel bijkomendArtikel, int aantal);
        int GetLastHuurcontractId();
        int GetNextHuurcontractId();
        List<Huurcontract> GetAll();
    }
}
