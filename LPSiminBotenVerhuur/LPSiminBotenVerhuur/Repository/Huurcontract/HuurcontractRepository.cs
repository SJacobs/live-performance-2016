﻿using LPSiminBotenVerhuur.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LPSiminBotenVerhuur.Repository
{
    public class HuurcontractRepository
    {
        private IHuurcontractContext context;

        public HuurcontractRepository()
        {
            context = new HuurcontractOracleContext();
        }

        public HuurcontractRepository(IHuurcontractContext context)
        {
            this.context = context;
        }

        public bool InsertHuurcontract(Huurcontract huurcontract)
        {
            return context.InsertHuurcontract(huurcontract);
        }

        public int GetLastHuurcontractId()
        {
            return context.GetLastHuurcontractId();
        }

        public int GetNextHuurcontractId()
        {
            return context.GetNextHuurcontractId();
        }

        public List<Huurcontract> GetAll()
        {
            return context.GetAll();
        }
    }
}
