﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LPSiminBotenVerhuur.Model
{
    /// <summary>
    /// Represents a person that hires an article.
    /// </summary>
    public class Huurder
    {
        public int Id { get; set; }

        /// <summary>
        /// Gets or sets the name.
        /// </summary>
        public string Naam { get; set; }

        /// <summary>
        /// Gets or sets the email.
        /// </summary>
        public string Email { get; set; }

        /// <summary>
        /// Initializes a new instance of the <see cref="Huurder"/> class.
        /// </summary>
        /// <param name="naam">Name of the person.</param>
        /// <param name="email">Email of the person.</param>
        public Huurder(int id, string naam, string email)
        {
            Id = id;
            Naam = naam;
            Email = email;
        }

        public Huurder(string naam, string email) : this(-1, naam, email)
        {
        }

        public override string ToString()
        {
            return string.Format("{0}, email: {1}", Naam, Email);
        }
    }
}
