﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LPSiminBotenVerhuur.Model
{
    /// <summary>
    /// Represents an product.
    /// </summary>
    public abstract class Artikel
    {
        /// <summary>
        /// Gets or sets the name of the product.
        /// </summary>
        /// <value>Name of product.</value>
        public string Naam { get; set; }

        /// <summary>
        /// Gets or sets the price of the product.
        /// </summary>
        /// <value>Price of product.</value>
        public decimal Prijs { get; set; }

        /// <summary>
        /// Initializes a new instance of the <see cref="Artikel"/> class.
        /// </summary>
        /// <param name="naam">Name of product.</param>
        /// <param name="prijs">Price of product.</param>
        public Artikel(string naam, decimal prijs)
        {
            Naam = naam;
            Prijs = prijs;
        }

        /// <summary>
        /// Gives a description of this product.
        /// </summary>
        /// <returns>Description of product.</returns>
        public override string ToString()
        {
            return string.Format("Naam: {0}, Prijs: {1}", Naam, Prijs);
        }
    }
}
