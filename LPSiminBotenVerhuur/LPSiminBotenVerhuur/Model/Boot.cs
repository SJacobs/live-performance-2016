﻿namespace LPSiminBotenVerhuur.Model
{
    /// <summary>
    /// Represents a boat, is also an article.
    /// </summary>
    public class Boot : Artikel
    {
        /// <summary>
        /// Gets or sets the id.
        /// </summary>
        /// <value>The id of the boat.</value>
        public int Id { get; set; }

        /// <summary>
        /// Gets or sets the type of boat.
        /// </summary>
        /// <value>The type of boat.</value>
        public BootType Type { get; set; }

        /// <summary>
        /// Gets or sets the tank volume.
        /// </summary>
        /// <value>The volume of boat.</value>
        public int TankInhoud { get; set; }

        /// <summary>
        /// Initializes a new instance of the <see cref="Boot"/> class.
        /// </summary>
        /// <param name="id">Id of the boat.</param>
        /// <param name="naam">Name of the boat.</param>
        /// <param name="prijs">Price of the boat.</param>
        /// <param name="type">Type of boat.</param>
        /// <param name="tankInhoud">Tank volume of boat.</param>
        public Boot(int id, string naam, decimal prijs, BootType type, int tankInhoud) : base(naam, prijs)
        {
            Id = id;
            Type = type;
            TankInhoud = tankInhoud;
        }

        /// <summary>
        /// Gets the action radius.
        /// </summary>
        /// <returns>Action radius.</returns>
        public int GetActieRadius()
        {
            return TankInhoud * 15;
        }

        /// <summary>
        /// Checks whether the boat is physically driven.
        /// </summary>
        /// <returns>Whether the boat is physically driven.</returns>
        public bool IsSpierkrachtAangedreven()
        {
            return Type == BootType.Kano || Type == BootType.ZeilbootLaser || Type == BootType.ZeilbootValk;
        }

        /// <summary>
        /// Gives a description of the boat.
        /// </summary>
        /// <returns>Description of boat.</returns>
        public override string ToString()
        {
            return string.Format("{0}, type: {1}, tank inhoud: {2}, prijs: € {3}", Naam, Type, TankInhoud, Prijs);
        }
    }
}
