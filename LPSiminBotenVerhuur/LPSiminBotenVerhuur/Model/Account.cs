﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LPSiminBotenVerhuur.Model
{
    /// <summary>
    /// Represents an account used to login.
    /// </summary>
    public class Account
    {
        /// <summary>
        /// Gets or sets the name.
        /// </summary>
        /// <value>The name of the account.</value>
        public string Naam { get; set; }

        /// <summary>
        /// Gets or sets the password.
        /// </summary>
        /// <value>The password of the account.</value>
        public string Wachtwoord { get; set; }

        /// <summary>
        /// Gets or sets the account role.
        /// </summary>
        /// <value>Role of account.</value>
        public AccountRol AccountRol { get; set; }

        /// <summary>
        /// Initializes a new instance of the <see cref="Account"/> class.
        /// </summary>
        /// <param name="naam">Name of account.</param>
        /// <param name="wachtwoord">Password of account.</param>
        /// <param name="accountRol">Account role.</param>
        public Account(string naam, string wachtwoord, AccountRol accountRol)
        {
            Naam = naam;
            AccountRol = accountRol;
        }
    }
}
