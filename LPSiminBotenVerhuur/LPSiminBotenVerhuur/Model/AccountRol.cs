﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LPSiminBotenVerhuur.Model
{
    /// <summary>
    /// Role of an account.
    /// </summary>
    public enum AccountRol
    {
        /// <summary>
        /// Administrator of the application.
        /// </summary>
        Administrator,

        /// <summary>
        /// Uses the application to rent articles.
        /// </summary>
        Medewerker,

        /// <summary>
        /// Account is a client.
        /// </summary>
        Klant
    }
}
