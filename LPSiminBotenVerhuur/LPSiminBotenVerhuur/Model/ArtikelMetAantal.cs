﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LPSiminBotenVerhuur.Model
{
    /// <summary>
    /// Product with an amount.
    /// </summary>
   public class ArtikelMetAantal
    {
        /// <summary>
        /// Gets or sets the product.
        /// </summary>
        /// <value>Product of product with amount.</value>
        public Artikel Artikel { get; set; }

        /// <summary>
        /// Gets or sets the amount.
        /// </summary>
        /// <value>The amount of products.</value>
        public int Aantal { get; set; }

        /// <summary>
        /// Initializes a new instance of the <see cref="ArtikelMetAantal"/> class.
        /// </summary>
        /// <param name="artikel">The product of the product with amount.</param>
        /// <param name="aantal">The amount of products.</param>
        public ArtikelMetAantal(Artikel artikel, int aantal)
        {
            Artikel = artikel;
            Aantal = aantal;
        }

        /// <summary>
        /// Gives a description of the product with amount.
        /// </summary>
        /// <returns>The description of product with amount.</returns>
        public override string ToString()
        {
            return Artikel + string.Format(", aantal: {0}", Aantal);
        }
    }
}
