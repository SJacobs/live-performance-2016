﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LPSiminBotenVerhuur.Model
{
    /// <summary>
    /// Represents an article that isn't a boat.
    /// </summary>
    public class BijkomendArtikel : Artikel
    {
        /// <summary>
        /// Gets or sets the id.
        /// </summary>
        /// <value>The id of the product.</value>
        public int Id { get; set; }

        /// <summary>
        /// Initializes a new instance of the <see cref="BijkomendArtikel"/> class.
        /// </summary>
        /// <param name="id">Id of product.</param>
        /// <param name="name">Name of product.</param>
        /// <param name="price">Price of product.</param>
        public BijkomendArtikel(int id, string name, decimal price) : base(name, price)
        {
            Id = id;
        }
    }
}
