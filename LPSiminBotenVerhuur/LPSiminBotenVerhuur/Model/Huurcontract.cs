﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LPSiminBotenVerhuur.Model
{
    /// <summary>
    /// Represents a rent contract.
    /// </summary>
    public class Huurcontract
    {
        /// <summary>
        /// Gets or sets the id of contract.
        /// </summary>
        /// <value>The id of the contract.</value>
        public int Id { get; set; }

        /// <summary>
        /// Gets or sets the person that setup the contract.
        /// </summary>
        /// <value>The name of the renter.</value>
        public string VerhuurderNaam { get; set; }

        /// <summary>
        /// Gets or sets the renter.
        /// </summary>
        /// <value>The renter of the contract.</value>
        public Huurder Huurder { get; set; }

        /// <summary>
        /// Gets or sets products.
        /// </summary>
        /// <value>The products of the contract.</value>
        public List<ArtikelMetAantal> Artikelen { get; set; }

        /// <summary>
        /// Gets or sets the start date.
        /// </summary>
        /// <value>The start date of contract.</value>
        public DateTime DatumStart { get; set; }

        /// <summary>
        /// Gets or sets the end date.
        /// </summary>
        /// <value>The end date of contract.</value>
        public DateTime DatumEinde { get; set; }

        /// <summary>
        /// Initializes a new instance of the <see cref="Huurcontract"/> class.
        /// </summary>
        /// <param name="id">Id of contract.</param>
        /// <param name="verhuurderNaam">Renter name of contract.</param>
        /// <param name="huurder">Hire of contract.</param>
        /// <param name="artikelen">Products of contract.</param>
        /// <param name="datumStart">Start date of contract.</param>
        /// <param name="datumEinde">End date of contract.</param>
        public Huurcontract(int id, string verhuurderNaam, Huurder huurder, List<ArtikelMetAantal> artikelen, DateTime datumStart, DateTime datumEinde)
        {
            Id = id;
            VerhuurderNaam = verhuurderNaam;
            Huurder = huurder;
            Artikelen = artikelen;
            DatumStart = datumStart;
            DatumEinde = datumEinde;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="Huurcontract"/> class.
        /// </summary>
        /// <param name="verhuurderNaam">Renter name of contract.</param>
        /// <param name="huurder">Hire of contract.</param>
        /// <param name="artikelen">Products of contract.</param>
        /// <param name="datumStart">Start date of contract.</param>
        /// <param name="datumEinde">End date of contract.</param>
        public Huurcontract(string verhuurderNaam, Huurder huurder, List<ArtikelMetAantal> artikelen, DateTime datumStart, DateTime datumEinde) : this(-1, verhuurderNaam, huurder, artikelen, datumStart, datumEinde)
        {
        }

        /// <summary>
        /// Export to text file.
        /// </summary>
        public void ExportToTxt()
        {
            string path = @"Huurcontract.txt";
            using (StreamWriter sw = new StreamWriter(path))
            {
                sw.WriteLine("Id: " + Id);
                sw.WriteLine("Verhuurder naam: " + VerhuurderNaam);
                sw.WriteLine("Huurder: " + Huurder.Naam);
                sw.WriteLine("Datum start: " + DatumStart);
                sw.WriteLine("Datum eind: " + DatumEinde);
                sw.WriteLine("Artikelen: ");

                foreach (ArtikelMetAantal artikel in Artikelen)
                {
                    sw.WriteLine("- " + artikel.Aantal + " x " + artikel.Artikel.Naam + " prijs: " + artikel.Artikel.Prijs);
                }
            }

            Process.Start(path);
        }

        /// <summary>
        /// Gives a description of the contract.
        /// </summary>
        /// <returns>Description of contract.</returns>
        public override string ToString()
        {
            return string.Format("Huurder: {0}, Datum start: {1}, Datum einde: {2}", Huurder.Naam, DatumStart, DatumEinde);
        }
    }
}
