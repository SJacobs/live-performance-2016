﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LPSiminBotenVerhuur.Model
{
    /// <summary>
    /// Feel temperature.
    /// </summary>
    public class GevoelsTemperatuur
    {
        /// <summary>
        /// Gets or sets the day.
        /// </summary>
        /// <value>The day of temperature.</value>
        public DateTime Dag { get; set; }

        /// <summary>
        /// Gets or sets the temperature.
        /// </summary>
        /// <value>The temperature.</value>
        public int Temperatuur { get; set; }

        /// <summary>
        /// Initializes a new instance of the <see cref="GevoelsTemperatuur"/> class.
        /// </summary>
        /// <param name="dag">Day of temperature.</param>
        /// <param name="temperatuur">Temperature currently.</param>
        public GevoelsTemperatuur(DateTime dag, int temperatuur)
        {
            Dag = dag;
            Temperatuur = temperatuur;
        }

        /// <summary>
        /// Gives a description of this object.
        /// </summary>
        /// <returns>The description.</returns>
        public override string ToString()
        {
            return string.Format("Dag: {0}, Temperatuur: {1}", Dag.ToString("dd/MM/yyyy"), Temperatuur);
        }
    }
}
