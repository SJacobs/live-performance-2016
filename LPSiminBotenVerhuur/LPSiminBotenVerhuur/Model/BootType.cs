﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LPSiminBotenVerhuur.Model
{
    /// <summary>
    /// The type of boat.
    /// </summary>
    public enum BootType
    {
        /// <summary>
        /// The boat is a canoe.
        /// </summary>
        Kano,

        /// <summary>
        /// The boat is a sailboat of a certain type.
        /// </summary>
        ZeilbootValk,

        /// <summary>
        /// The boat is a sailboat of a certain type.
        /// </summary>
        ZeilbootLaser,

        /// <summary>
        /// The boat is a motorboat of a certain type.
        /// </summary>
        MotorbootKruiser
    }
}
